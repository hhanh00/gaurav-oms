from django.db import models

class Order(models.Model):
  name = models.IntegerField()

  def __str__(self): return self.name
